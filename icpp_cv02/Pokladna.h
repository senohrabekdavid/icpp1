#ifndef ICPP_CV02_POKLADNA_H
#define ICPP_CV02_POKLADNA_H


#include "Uctenka.h"

class Pokladna {
public:
    Pokladna();
    ~Pokladna();

    Uctenka& vystavUctenku(double aCastka, double aDph);
    Uctenka& dejUctenku(int aCislo);
    double dejCastku() const;
    double dejCastkuVcDph() const;

private:
    Uctenka* poleUctenek;
    static int citacId;
    int pocetVydanychUctenek=0, ciselnaRada = 1000;


};



#endif //ICPP_CV02_POKLADNA_H

