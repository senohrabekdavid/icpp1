#ifndef ICPP_CV02_UCTENKA_H
#define ICPP_CV02_UCTENKA_H
#pragma once


class Uctenka {

public:

    Uctenka(){};
    ~Uctenka(){};

    int getCisloUctenky() const;

    void setCisloUctenky(const int aCisloUctenky);

    double getCastka() const;

    void setCastka(double aCastka);

    double getDph() const;

    void setDph(double aDph);

private:
    int cisloUctenky;
    double castka, dph;

};


#endif //ICPP_CV02_UCTENKA_H
