
//
// Created by david on 13.10.2020.
//

#include "Pokladna.h"

Pokladna::Pokladna() {
    poleUctenek = new Uctenka[10];
}

Pokladna::~Pokladna() {
    delete[] poleUctenek;
}

Uctenka& Pokladna::vystavUctenku(double aCastka, double aDph){
    poleUctenek[pocetVydanychUctenek].setCisloUctenky(ciselnaRada++);
    poleUctenek[pocetVydanychUctenek].setCastka(aCastka);
    poleUctenek[pocetVydanychUctenek].setDph(aDph);
    pocetVydanychUctenek++;
    return poleUctenek[pocetVydanychUctenek];
}
Uctenka& Pokladna::dejUctenku(int aCislo){
    for (int i = 0; i < pocetVydanychUctenek ; ++i) {
        if(poleUctenek[i].getCisloUctenky() == aCislo)
            return poleUctenek[i];
    }
}
double Pokladna::dejCastku() const{
    double total=0;
    for (int i = 0; i < pocetVydanychUctenek; ++i) {
        total += poleUctenek[i].getCastka();
    }
    return total;
}
double Pokladna::dejCastkuVcDph() const{
    double total=0;
    for (int i = 0; i < pocetVydanychUctenek; ++i) {
        total += poleUctenek[i].getDph();
    }
    return  total;
}

