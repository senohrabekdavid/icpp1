#include "Uctenka.h"

int Uctenka::getCisloUctenky() const {
    return cisloUctenky;
}

void Uctenka::setCisloUctenky(const int aCisloUctenky) {
    cisloUctenky = aCisloUctenky;
}

double Uctenka::getCastka() const {
    return castka;
}

void Uctenka::setCastka(const double aCastka) {
    castka = aCastka;
}

double Uctenka::getDph() const {
    return dph;
}

void Uctenka::setDph(const double aDph) {
    dph = aDph;
}
