#pragma once
#include <iostream>

namespace Entity
{
    class Person
    {
    public:
        Person() {};
        Person(std::string aName, std::string aPhone, int aId);
        ~Person();
        std::string getName() const;
        std::string getPhone() const;
        int getId() const;
    private:
        int _id;
        std::string _name;
        std::string _phone;
    };
}

