#include "Person.h"

namespace Entity {
    Person::Person(std::string aName, std::string aPhone, int aId) {
         Person::_name = aName;
         Person::_phone = aPhone;
         Person::_id = aId;
    }

    std::string Person::getName() const {
        return _name;
    }

    int Person::getId() const {
        return _id;
    }

    std::string  Person::getPhone() const {
        return _phone;
    }

    Entity::Person::~Person()
    {
    }

}
