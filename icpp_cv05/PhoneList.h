#include <string>
#include "Person.h"

namespace Model {
    class PhoneList
    {
    public:
        PhoneList();
        ~PhoneList();
        void addPerson(Entity::Person aPerson);
        std::string findPhone(std::string aName) const;
        std::string findPhone(int aId)const;
    private:
        class Element
        {
        public:
            Element();
            ~Element();
            Entity::Person _data;
            Element* _next;
        };

        Element* _start;

    };
}

