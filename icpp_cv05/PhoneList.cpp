#include "PhoneList.h"

using namespace Model;

PhoneList::PhoneList(){
    _start = nullptr;
}

PhoneList::~PhoneList(){
    Element* item = _start;
    while (item != nullptr) {
        Element* temporary = item->_next;
        delete item;
        item = temporary;
    }

    _start = nullptr;
}

void PhoneList::addPerson(Entity::Person aPerson){
    Element* item = new Element();
    item->_data = aPerson;
    item->_next = _start;
    _start = item;
}

std::string PhoneList::findPhone(std::string aName) const {
    if (aName.size() == 0) {
        throw std::invalid_argument("Please enter a valid name.");
    }

    Element* item = _start;

    while (item != nullptr) {
        if (item->_data.getName() == aName) {
            return item->_data.getPhone();
        }
        item = item->_next;
    }
    throw std::invalid_argument("User not found!");
}

std::string PhoneList::findPhone(int aId) const {
    if (aId < 0) {
        throw std::invalid_argument("Please enter id > 0");
    }

    Element* item = _start;

    while (item != nullptr) {
        if (item->_data.getId() == aId) {
            return item->_data.getPhone();
        }
        item = item->_next;
    }
    throw std::invalid_argument("User not found!");
}

