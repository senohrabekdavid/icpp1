#pragma once
#include "Objekt.h"
#include <string>
#include <stdio.h>

class PohyblivyObjekt : public Objekt
{
private:
    double uhelNatoceni;
public:
    PohyblivyObjekt(int id) : Objekt(id) { uhelNatoceni = 0; }

    void setUhelNatoceni(double aUhelNatoceni) {
        uhelNatoceni = aUhelNatoceni;
    }
    double getUhelNatoceni() const {
        return uhelNatoceni;
    }
    std::string toString() const {
        return "PohyblivyObjekt, uhelNatoceni: " + std::to_string(uhelNatoceni);
    }
};

