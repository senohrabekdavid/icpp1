#pragma once
#include "PohyblivyObjekt.h"

class Monstrum : public PohyblivyObjekt
{
private:
    int hp;
    int maxHp;
public:
    Monstrum(int aId, int aHp, int aMaxHp) : PohyblivyObjekt(aId) {
        hp = aHp;
        maxHp = aMaxHp; }

    void setHp(int aHp) {
        hp = aHp;
    }

    double getHp() const {
        return hp;
    }

    void setMaxHp(int aMaxHp) {
        maxHp = aMaxHp;
    }

    double getMaxHp() const {
        return maxHp;
    }
};

