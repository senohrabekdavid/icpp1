#pragma once
class Objekt
{
private:
    int id;
    double x;
    double y;
public:
    Objekt(int aId) {
        id = aId;
        x = 0;
        y = 0;
    }

    virtual ~Objekt() { }

    int getId() const {
        return id;
    }

    void setX(double aX) {
        x = aX;
    }
    int getX() const {
        return  x;
    }

    void setY(double aY) {
        y = aY;
    }
    int getY() const {
        return  y;
    }

};

