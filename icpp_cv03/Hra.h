#pragma once
#include <stdexcept>
#include "Objekt.h"
#include "PohyblivyObjekt.h"
#include "StatickyObjekt.h"
class Hra
{
private:
    static int MAX_OBJEKT;
    static int OBJEKT_COUNT;
    Objekt** objekty;

public:
    Hra();
    ~Hra();
    void vlozObjekt(Objekt* o);
    int* najdiIdStatickychObjektu(double xmin, double xmax, double ymin, double ymax) const;
    PohyblivyObjekt** najdiPohybliveObjektyVOblasti(double x, double y, double r) const;
    PohyblivyObjekt** najdiPohybliveObjektyVOblasti(double x, double y, double r, double umin, double umax) const;

};

