//
// Created by david on 20.10.2020.
//

#include "Hra.h"

int Hra::MAX_OBJEKT = 20;
int Hra::OBJEKT_COUNT = 0;

Hra::Hra()
{
    objekty = new Objekt*[MAX_OBJEKT];
}

Hra::~Hra()
{
    if(objekty != nullptr){
        for (int i = 0; i < MAX_OBJEKT; ++i){
            if(objekty[i] != nullptr){
                delete objekty[i];
            }
        }
        delete[] objekty;
    }

}


void Hra::vlozObjekt(Objekt* aObjekt)
{
    if (OBJEKT_COUNT + 1 > MAX_OBJEKT)
    {
        throw std::out_of_range("Přetečení pole");
    } else {
        objekty[OBJEKT_COUNT++] = aObjekt;
    }
}
int* Hra::najdiIdStatickychObjektu(double aXmin, double aXmax, double aYmin, double aYmax) const
{
    int* pole = new int;
    int pocitadlo = 0;
    for (int i = 0; i < OBJEKT_COUNT; i++)
    {
        StatickyObjekt* so = dynamic_cast<StatickyObjekt*>(objekty[i]);
        if (so != nullptr && (so->getX()>=aXmin && so->getX() <= aXmax && so->getY() >= aYmin && so->getY() <= aYmax))
        {
            pole[pocitadlo] = so->getId();
            pocitadlo++;
        }
    }

    if (pocitadlo == 0)
    {
        return nullptr;
    } else {
        return pole;
    }

}





PohyblivyObjekt** Hra::najdiPohybliveObjektyVOblasti(double aX, double aY, double aR) const
{
    //Ask: Jak udělat dynamické pole objektů typu PohyblivyObjekt? (

    int pocitadlo = 0;
    for (int i = 0; i < OBJEKT_COUNT; i++)
    {
        PohyblivyObjekt* po = dynamic_cast<PohyblivyObjekt*>(objekty[i]);
        if (po != nullptr && pow(po->getX() - aX, 2) + pow(po->getY() - aY, 2) < pow(aR, 2))
        {
            pocitadlo++;
        }
    }

    if (pocitadlo == 0)
    {
        return nullptr;
    }

    PohyblivyObjekt** poleVystupu = new PohyblivyObjekt * [pocitadlo];
    int pocitadloVystupu = 0;
    for (int i = 0; i < OBJEKT_COUNT; i++)
    {
        PohyblivyObjekt* po = dynamic_cast<PohyblivyObjekt*>(objekty[i]);
        if (po != nullptr && pow(po->getX() - aX, 2) + pow(po->getY() - aY, 2) < pow(aR, 2))
        {
            poleVystupu[pocitadloVystupu++] = po;
        }
    }

    return poleVystupu;
}

PohyblivyObjekt** Hra::najdiPohybliveObjektyVOblasti(double aX, double aY, double aR, double aUmin, double aUmax) const
{
    int pocitadlo = 0;
    for (int i = 0; i < OBJEKT_COUNT; i++)
    {
        PohyblivyObjekt* po = dynamic_cast<PohyblivyObjekt*>(objekty[i]);
        if (po != nullptr && pow(po->getX() - aX, 2) + pow(po->getY() - aY, 2) < pow(aR, 2) && po->getUhelNatoceni() > aUmin && po->getUhelNatoceni() < aUmax)
        {
            pocitadlo++;
        }
    }

    if (pocitadlo == 0)
    {
        return nullptr;
    }

    PohyblivyObjekt** poleVystupu = new PohyblivyObjekt * [pocitadlo];
    int pocitadloVystupu = 0;
    for (int i = 0; i < OBJEKT_COUNT; i++)
    {
        PohyblivyObjekt* po = dynamic_cast<PohyblivyObjekt*>(objekty[i]);
        if (po != nullptr && pow(po->getX() - aX, 2) + pow(po->getY() - aY, 2) < pow(aR, 2) && po->getUhelNatoceni() > aUmin && po->getUhelNatoceni() < aUmax)
        {
            poleVystupu[pocitadloVystupu++] = po;
        }
    }

    return poleVystupu;
}

