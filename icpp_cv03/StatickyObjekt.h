
#pragma once
#include "Objekt.h"
enum class TypPrekazky { Skala, MalaRostlina, VelkaRostlina};

class StatickyObjekt: public Objekt
{
private:
    TypPrekazky typPrekazky;

public:
    StatickyObjekt(int aId, TypPrekazky aTypPrekazky) : Objekt(aId) {
        this->typPrekazky = aTypPrekazky;
    }

    TypPrekazky GetObstacle() const {
        return typPrekazky;
    }
};

