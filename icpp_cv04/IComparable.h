#pragma once
#include "IObject.h"

class IComparable : public IObject
{
public:
    virtual int compareTo(IComparable* aObject) const = 0;
};
