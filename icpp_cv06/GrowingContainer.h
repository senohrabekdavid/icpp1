#include <stdexcept> 

template <class dataType, int InitialSize = 5, int RisingCoefficient = 2>

class GrowingContainer 
{
public:
	GrowingContainer();
	~GrowingContainer();
	bool isAFreeElement() const;
	void MagnifyArray();
	void addElement(const dataType& o);
	dataType& operator[] (int index);
	dataType operator[] (int index) const;
	unsigned int quantity() const;
private:
	dataType* _array;
	unsigned _arraySize;
	unsigned _quantityOfCorrectElements;

};

template <typename dataType, int InitialSize, int RisingCoefficient>
GrowingContainer<dataType, InitialSize, RisingCoefficient>::GrowingContainer()
{
	this->_arraySize = InitialSize;
	this->_array = new dataType[_arraySize];
	_quantityOfCorrectElements = 0;
}

template<typename dataType, int InitialSize, int RisingCoefficient>
 GrowingContainer<dataType, InitialSize, RisingCoefficient>::~GrowingContainer()
{
	 delete[] _array;
}

 template<typename dataType, int InitialSize, int RisingCoefficient>
  bool GrowingContainer<dataType, InitialSize, RisingCoefficient>::isAFreeElement() const
 {
		  return _quantityOfCorrectElements == _arraySize;
 }

  template<typename dataType, int InitialSize, int RisingCoefficient>
 void GrowingContainer<dataType, InitialSize, RisingCoefficient>::MagnifyArray()
  {
		 int pomVel = RisingCoefficient * _arraySize;
		 dataType* pom = new dataType[pomVel];


		 for (size_t i = 0; i < _arraySize; i++)
		 {
			 pom[i] = _array[i];


		 }
		 delete[] _array;
		 _array = pom;
		 _arraySize = pomVel;
  }

 template<typename dataType, int InitialSize, int RisingCoefficient>
 void GrowingContainer<dataType, InitialSize, RisingCoefficient>::addElement(const dataType& o)
 {
	 if (!isAFreeElement())
	 {
		 MagnifyArray();
	 }
		
	 _array[_quantityOfCorrectElements++] = o;


 }

 template<typename dataType, int InitialSize, int RisingCoefficient>
 dataType& GrowingContainer<dataType, InitialSize, RisingCoefficient>::operator[](int index)
 {
	 if (index < _quantityOfCorrectElements)
	 {
		 return _array[index];
	 }
	 else
	 {
		 throw std::invalid_argument("Out of range");
	 }
 }

 template<typename dataType, int InitialSize, int RisingCoefficient>
dataType GrowingContainer<dataType, InitialSize, RisingCoefficient>::operator[](int index) const
 {
	 if (index < _quantityOfCorrectElements)
	 {
		 return _array[index];
	 }
	 else
	 {
		 throw std::invalid_argument("Invalid index");
	 }
 }

 template<typename dataType, int InitialSize, int RisingCoefficient>
 unsigned int GrowingContainer<dataType, InitialSize, RisingCoefficient>::quantity() const
 {
	 return this->_quantityOfCorrectElements;
 }

