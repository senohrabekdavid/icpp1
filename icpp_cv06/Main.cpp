#include "GrowingContainer.h"
#include <iostream>
using namespace std;
int main()
{

    GrowingContainer<int> container ;
    container.addElement(5);
    container.addElement(4);
    container.addElement(3);
    container.addElement(2);
    container.addElement(1);
    container.addElement(6);
 
    try
    {
    cout << "Number of elements in the container: " << container.pocet()<< endl;
    cout << container[1] << endl;
    }
    catch (const std::exception e)
    {
        cout << e.what() << endl;
    }
    return 0;
}

