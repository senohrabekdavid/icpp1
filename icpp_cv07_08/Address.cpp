#include "Address.h"

Address::Address(string _street, string _city, int _zip)
{
	this->_street = _street;
	this->_city = _city;
	this->_zip = _zip;
}

std::ostream& operator<<(std::ostream& os, const Address& address)
{
	return os << address._street << ", " << address._city << ", " << address._zip;
}

std::istream& operator>>(std::istream& is, Address& address)
{	
	return is >> address._street >> address._city >> address._zip;
}