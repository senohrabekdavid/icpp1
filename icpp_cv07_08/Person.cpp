#include "Person.h"

Person::Person(string _name, string _lastName, Address _address, Date _birthDate)
{
    this->_name = _name;
    this->_lastName = _lastName;
    this->_address = _address;
    this->_birthDate = _birthDate;
}

std::ostream& operator<<(std::ostream& os, const Person& person)
{
    return os << person._name << " " << person._lastName << ", " << person._address << ", " << person._birthDate;
}

std::istream& operator>>(std::istream& is, Person& person)
{
    char ch;
    is >> person._name >> person._lastName >> ch >> person._address >> ch >> person._birthDate;
    return is;
}

