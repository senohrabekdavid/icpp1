#include <string>
#include <iostream>
using namespace std;

struct Address
{
private:
	string _street;
	string _city;
	int _zip;
public:
	Address();
	~Address();
	Address(string _street, string _city, int _zip);
	friend std::ostream& operator<<(std::ostream& os, const Address& address);
	friend std::istream& operator>>(std::istream& is, Address& address);
};