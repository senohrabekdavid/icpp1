#pragma once

#include <exception>
#include <iostream>

template <class T>
class Matrix
{
public:
    Matrix(int aRows, int aColumns);
    Matrix(const Matrix<T>& m);
    ~Matrix();
    void Set(int aRow, int aCol, T aValue);
    void SetZ(T* aArray);
    T& Get(int aRow, int aCol);
    const T& Get(int aRow, int aCol) const;
    template <class R>
    Matrix<R> CastTo() const;
    Matrix Transposition() const;
    Matrix Multiple(const Matrix& aM) const;
    Matrix Multiple(T aScalar) const;
    Matrix Sum(const Matrix& aM) const;
    Matrix Sum(T aScalar) const;
    bool IsEqual(const Matrix& aM) const;
    int GetRows() const { return this->rows; }
    void SetRows(int aRows) { this->rows = rows; }
    int GetColumns() const { return this->columns; }
    void SetColumns(int aColumns) { this->rows = columns; }

private:
    int rows, columns;
    T** array;
    bool IsInvalidIndex(int aRow, int aCol) const;
    void InitializeMatrix(int aRows, int aColumns);
};

template<class T>
void Matrix<T>::InitializeMatrix(int aRows, int aColumns)
{
    this->columns = aColumns;
    this->rows = aRows;
    this->array = new T*[rows];

    for (int i = 0; i < rows; i++)
        array[i] = new T[columns];
}

template<class T>
bool Matrix<T>::IsInvalidIndex(int aRow, int aColumn) const
{
    return aRow < 0 && aRow >= this->rows
           || aColumn < 0 && aColumn >= this->columns;
}

template<class T>
Matrix<T>::Matrix(int aRows, int aColumns)
{
    InitializeMatrix(aRows, aColumns);
}

template<class T>
Matrix<T>::Matrix(const Matrix<T>& aM)
{
    InitializeMatrix(aM.GetRows(), aM.GetColumns());

    for (int i = 0; i < aM.GetRows(); i++)
        for (int j = 0; j < aM.GetColumns(); j++)
            array[i][j] = aM.Get(i, j);
}

template<class T>
Matrix<T>::~Matrix()
{
    for (int i = 0; i < rows; i++)
        delete[] array[i];
    delete[] array;
}

template<class T>
void Matrix<T>::Set(int aRow, int aColumn, T aValue)
{
    if(IsInvalidIndex(aRow, aColumn))
        throw out_of_range("Index is out of range.");

    array[aRow][aColumn] = aValue;
}

template<class T>
void Matrix<T>::SetZ(T* aArray)
{
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < columns; j++)
            this->array[i][j] = aArray[(columns * i) + j];
}

template<class T>
T& Matrix<T>::Get(int aRow, int aColumn)
{
    if (IsInvalidIndex(aRow, aColumn))
        throw out_of_range("Index is out of range");

    return array[aRow][aColumn];

}

template<class T>
const T& Matrix<T>::Get(int aRow, int aColumn) const
{
    if (IsInvalidIndex(aRow, aColumn))
        throw out_of_range("Index is out of range");

    return array[aRow][aColumn];
}

template <class T>
template <class R>
Matrix<R> Matrix<T>::CastTo() const
{
    Matrix<R> output(rows, columns);
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < columns; j++)
            output.Set(i, j, (R)(array[i][j]));

    return output;
}

template<class T>
Matrix<T> Matrix<T>::Transposition() const
{
    Matrix output(rows, columns);
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < columns; j++)
            output.Set(j, i, array[i][j]);

    return output;
}

template<class T>
Matrix<T> Matrix<T>::Multiple(const Matrix& aM) const
{
    if(columns != aM.GetRows())
        throw std::exception("Exception - matrix size");

    Matrix<T> output(rows, columns);
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < aM.GetColumns(); j++)
        {
            T temp = T();
            for (int k = 0; k < columns; k++)
                temp += array[i][k] * aM.Get(k, j);
            output.Set(i, j, temp);
        }

    return output;
}

template<class T>
Matrix<T> Matrix<T>::Multiple(T aScalar) const
{
    Matrix<T> output;
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < columns; j++)
            output.Set(i,j, array[i][j] * aScalar);

    return output;
}

template<class T>
Matrix<T> Matrix<T>::Sum(const Matrix& aM) const
{
    if (aM.GetColumns() != columns && aM.GetRows() != rows)
        throw exception("Exception - matrix size");

    Matrix<T> output(rows, columns);
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < columns; j++)
            output.Set(i,j) = array[i][j] + aM.Get(i, j);

    return output;
}

template<class T>
Matrix<T> Matrix<T>::Sum(T aScalar) const
{
    Matrix<T> output(rows, columns);
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < columns; j++)
            output.Set(i,j, array[i][j] + aScalar);

    return output;
}

template<class T>
bool Matrix<T>::IsEqual(const Matrix& aM) const
{
    if (aM.GetColumns() != columns && aM.GetRows() != rows)
        throw exception("Exception - matrix size");

    for (int i = 0; i < rows; i++)
        for (int j = 0; j < columns; j++)
            if (array[i][j] != aM.Get(i, j))
                return false;
    return true;
}


